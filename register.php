<html>
<head>
  <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
  <?php session_start(); ?>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a href=""><img src="img/flor.jpg" height="60p"></a>
      </div>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="login.php">Ingresar</a></li>
        <li ><a href="register.php">Registrar</a></li>
      </ul>
    </div>
    </div>
  </nav>

<form class="form-horizontal" action="procesos/p_validMail2.php" method="POST">
  <center>
    <?php  if (isset($_SESSION['mensaje'])) { ?>
    <div class="alert alert-danger" role="alert"><?php echo $_SESSION['mensaje'] ?></div>
    <?php 
    session_unset();
    }
  ?> 
  <table>
  <fieldset>
    <legend>Registro - Register</legend>
    <div class="form-group">
      <label >Email:</label>
      <input type="email"  placeholder="Correo Institucional" required="" name="txtCorreo" id="txtCorreo">
    </div>
    <div class="form-group">
        <button type="reset" class="btn btn-default">Cancelar</button>
        <button type="submit" class="btn btn-warning">Registrar</button>
    </div>
  </fieldset>
</table>
<?php include 'system/messages.php'; ?>
</center>
</form>

</body>
</html>