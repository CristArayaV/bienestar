<?php  
if(!isset($_SESSION)) {
     session_start();
}
include 'conexion/cone.php';
$cone = new cone();
?>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
        <a href="index.php"><img src="img/flor.jpg" height="60p"></a>
      </div>

<?php
  if (isset($_SESSION['usuario'])) {
    if ($_SESSION['tip']=='6') { ?>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li ><a href="">Inicio<span class="sr-only">(current)</span></a></li>
        <li ><a href="registrarPrac.php">Registrar Practicante<span class="sr-only">(current)</span></a></li>
        <li ><a href="">op2<span class="sr-only">(current)</span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="procesos/p_logouth.php">Cerrar Sesión<span class="sr-only">(current)</span></a></li>
      </ul>
    </div>
  </div>
</nav>
    <?php
  }
  elseif ($_SESSION['tip']=='4') {
?>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li ><a href="home.php">Inicio<span class="sr-only">(current)</span></a></li>
        <li ><a href="pra.php">Practicantes<span class="sr-only">(current)</span></a></li>
        <li ><a href="registrarPrac.php">Registrar Practicante<span class="sr-only">(current)</span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="estadisticas.php">Estadisticas</a></li>
        <li ><a href="procesos/p_logouth.php">Cerrar Sesión<span class="sr-only">(current)</span></a></li>
      </ul>
    </div>
  </div>
</nav>
<?php
  }
  elseif ($_SESSION['tip']=='5') {
    ?>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li ><a href="index.php">Inicio<span class="sr-only">(current)</span></a></li>
        <li ><a href="home.php">Registro Horas<span class="sr-only">(current)</span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="procesos/p_logouth.php">Cerrar Sesión<span class="sr-only">(current)</span></a></li>
      </ul>
    </div>
  </div>
</nav>
<?php  
  }
  elseif($_SESSION['tip']=='1'||$_SESSION['tip']=='3'||$_SESSION['tip']=='3'){?>
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li ><a href="">Inicio<span class="sr-only">(current)</span></a></li>
        <li ><a href="">opcion2<span class="sr-only">(current)</span></a></li>
        <li ><a href="">opcion3<span class="sr-only">(current)</span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="procesos/p_logouth.php">Cerrar Sesión<span class="sr-only">(current)</span></a></li>
      </ul>
    </div>
  </div>
</nav>

<?php
  }
 ?>

<!--Home
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li ><a href="index.php">Inicio<span class="sr-only">(current)</span></a></li>
        <li ><a href="pra.php">Practicantes<span class="sr-only">(current)</span></a></li>
        <li ><a href="ficha.php">Ficha<span class="sr-only">(current)</span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="estadisticas.php">Estadisticas</a></li>
        <li ><a href="procesos/p_logouth.php">Cerrar Sesión<span class="sr-only">(current)</span></a></li>
      </ul>
    </div>
  </div>
</nav>
-->
<?php 
  }
  else
  {
    header("Location: login.php");
  }

?>


