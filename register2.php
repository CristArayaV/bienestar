<html>
<head>
  <link rel="stylesheet" href="css/bootstrap.css">
  <script type="text/javascript" src="js/jquery.js"></script>
</head>

<body>

<?php  
    session_start();
    $correo=$_SESSION['core'];
    $tipo=$_SESSION['tipo'];
?>
<nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a href=""><img src="img/flor.jpg" height="60p"></a>
      </div>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="login.php">Ingresar</a></li>
        <li ><a href="register.php">Registrar</a></li>
      </ul>
    </div>
    </div>
  </nav>

<!--<script type="text/javascript">
  $(document).ready(function(event){
    $("#btnReg").click(function(){
      $.ajax({
        type:'POST',
        url:'procesos/p_registro2.php',
        data:$("#formRegistro").serialize(),
        success:function(data){
          alert('OK');
        }
      });
    });
  });

</script>

-->
<form class="form-horizontal" id="formRegistro" action="procesos/p_registro2.php" method="POST">
<table>
  <center>
    
    <legend>Registro - Register</legend>
    <div class="form-group">
      <label >Email:</label>
      <input type="email" required="" disabled="" name="mail" value="<?php echo $correo; ?>">
      <br>
      <label>Nombre</label>
      <input type="text" name="nom" required="" placeholder="Primer Nombre">
      <br>
      <label>Apellidos</label>
      <input type="text" name="ape" required="" placeholder="Apellidos">
      <br>
      <label>Rut: </label>
      <input type="text" name="run" placeholder="18888888" required="" size="7" >
      <input type="text" name="digv" placeholder="K" required="" size="1" >
      <br>
      <label>Telefono: +56</label>
      <input type="number" name="fono" placeholder="9 78995214" required="" size="12">
      <br>
      <label >Contraseña: </label>
      <input type="password"  name="contra" required="" placeholder="ingrese Contraseña">
      <br>
      <label>Confirmar Contraseña</label>
      <input type="password" name="rcontra" placeholder="confirmar contraseña" required="">
      <br>
      <input type="hidden" name="ti" value="<?php echo $tipo ?>" disabled="">
    </div>
    <div class="form-group">
        <button name="btnReg"  name="btnReg" class="btn btn-info" value="Registrar">Registrar</button> 
        <a href="register.php" class="btn btn-danger">Cancelar</a>
    </div>

    </center>
</table>
  <?php 
  include 'system/messages.php';
  $_SESSION['mail']=$correo; 
  $_SESSION['tip']=$tipo; 
  ?>
</form>
</body>
</html>