<html>
<head>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">

  <title>Bienestar Palto</title>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a href=""><img src="img/flor.jpg" height="60p"></a>
      </div>
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="login.php">Ingresar</a></li>
        <li ><a href="register.php">Registrar</a></li>
      </ul>
    </div>
    </div>
  </nav>

<form class="form-horizontal" method="post" action="procesos/p_login.php">
   
  <table>
    <center>

  <?php  if (isset($_SESSION['mensaje'])) { ?>
    <div class="alert alert-danger" role="alert"><?php echo $_SESSION['mensaje'] ?></div>
    <?php 
    session_unset();
  }
  ?> 

    <legend>Ingreso - Login</legend>
    <div class="form-group">
      <label>Correo:</label>
      <input type="email" id="txtCorreo" name="txtCorreo" placeholder="correo Institucional" required="">
    </div>
    <div class="form-group">
      <label>Clave:</label>&nbsp;&nbsp;
      <input type="password" id="txtPass" name="txtPass" placeholder="Clave" required="">
    </div>
    <div class="form-group">
      <a class="btn btn-info">Olvide mi clave</a>
      <button type="submit" class="btn btn-success" id="btn-login" name="btn-login">Ingresar</button>
    </div>
    </center>
  </table>
  
</form>
</body>
</html>