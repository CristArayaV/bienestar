<html>
<head>
  <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
<?php  
  include 'navMenu.php';
if (!$_SESSION['tip']=="4") {
  header("Location: login.php");  
}
  ?>

<table>
  <center>
    <form class="form-horizontal" action="procesos/p_registro.php" method="POST">

    <legend>Registro Funcionario- Employee Register </legend>
    <div class="form-group">
      <label >Email:</label>
      <input type="email" required="" name="mail" placeholder="mail@mail.com">
      <br>
      <label>Nombre</label>
      <input type="text" name="nom" required="" placeholder="ingrese Nombre">
      <br>
      <label>Rut: </label>
      <input type="text" name="run" placeholder="18.888.888" required="">
      <input type="text" name="digV" placeholder="K" required="" size="1">
      <br>
      <label >Contraseña: </label>
      <input type="password"  name="contra" required="" placeholder="ingrese Contraseña">
      <br>
      <label >Confirmar Contraseña: </label>
      <input type="rPass" name="rcontra" placeholder="confirmar contraseña" required="">
    </div>
    <div class="form-group">
        <input type="submit" name="btnReg" class="btn btn-info" value="Registrar">
        <a href="register.php" class="btn btn-danger">Cancelar</a>
    </div>

  </form>
  </center>
</table>
</body>
</html>