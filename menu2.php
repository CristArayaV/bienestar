<html>
<head>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="con/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="con/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="con/_bootswatch.scss">
  <link rel="stylesheet" type="text/css" href="con/_variables.scss">

<body>
  <?php 
  include 'navMenu.php';

  ?>
<nav>
  <div style="text-align:center;padding:1em 0;"> 
      <iframe src="https://www.zeitverschiebung.net/clock-widget-iframe-v2?language=es&size=small&timezone=America%2FSantiago" width="100%" height="90" 
      frameborder="0" seamless></iframe> 
  </div>
</nav>

<div class='container'>
<table class="table table-striped table-hover table-bordered">
  <thead class="thead-dark">
    <tr>
      <th>#</th>
      <th>Rut</th>
      <th>Paciente</th>
      <th>Hora</th>
      <th>Tipo Secion</th>
      <th>Alumno</th>
      <th>box</th>
      <th></th>
      
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td><input type="text" name="run" placeholder="12345678" required="" "></td>
      <td>"Paciente"</td>
      <td>16:00</td>
      <td><select class="custom-select">
        <option>Seleccione</option>
        <option>Masoterapia</option>
        <option>Auriculoterapia</option>
        <option>Pedras Calientes</option>
        <option>Reflexologia</option>
      </select></td>
      <td>Maria Valdes</td>
      <td><label><input type="submit" id="1" value="1" class="btn btn-primary"></label> &nbsp
          <label><input type="submit" id="2" value="2" class="btn btn-success"></label> &nbsp
          <label><input type="submit" id="3" value="3" class="btn btn-info"></label></td>
      <td>
        <input type="submit" name="agregar" value="Agregar">&nbsp
        <input type="submit" name="eliminar" value="Eliminar">&nbsp
        <input type="submit" name="modificar" value="Modificar">
      </td>
    </tr>
    <tr>
      <td>2</td>
      <td><input type="text" name="run" placeholder="12345678" required="" "></td>
      <td>"Paciente"</td>
      <td>16:00</td>
      <td><select class="custom-select">
        <option>Seleccione</option>
        <option>Masoterapia</option>
        <option>Auriculoterapia</option>
        <option>Pedras Calientes</option>
        <option>Reflexologia</option>
      </select></td>
      <td>Francisca Campos</td>
      <td><label><input type="submit" id="cbox1" value="1" class="btn btn-primary"></label> &nbsp
          <label><input type="submit" id="cbox1" value="2" class="btn btn-success"></label> &nbsp
          <label><input type="submit" id="cbox1" value="3" class="btn btn-info"></label>
      </td>
      <td>
        <input type="submit" name="agregar" value="Agregar">&nbsp
        <input type="submit" name="eliminar" value="Eliminar">&nbsp
        <input type="submit" name="modificar" value="Modificar">
      </td>
    </tr>
    <tr class="table-info">
      <td>3</td>
      <td><input type="text" name="run" placeholder="12345678" required="" "></td>
      <td>"Paciente"</td>
      <td>16:00</td>
      <td><select class="custom-select">
        <option>Seleccione</option>
        <option>Masoterapia</option>
        <option>Auriculoterapia</option>
        <option>Pedras Calientes</option>
        <option>Reflexologia</option>
      </select></td>
      <td>Estefania Rubilar</td>
      <td><label><input type="submit" id="cbox1" value="1" class="btn btn-primary"></label> &nbsp
          <label><input type="submit" id="cbox1" value="2" class="btn btn-success"></label> &nbsp
          <label><input type="submit" id="cbox1" value="3" class="btn btn-info"></label>
      </td>
      <td>
        <input type="submit" name="agregar" value="Agregar">&nbsp
        <input type="submit" name="eliminar" value="Eliminar">&nbsp
        <input type="submit" name="modificar" value="Modificar">
      </td>
    </tr>
    <tr>
      <td>4</td>
      <td><input type="text" name="run" placeholder="12345678" required="" "></td>
      <td>"Paciente"</td>
      <td>16:30</td>
      <td><select class="custom-select">
        <option>Seleccione</option>
        <option>Masoterapia</option>
        <option>Auriculoterapia</option>
        <option>Pedras Calientes</option>
        <option>Reflexologia</option>
      </select></td>
      <td>Maria Valdes</td>
      <td><label><input type="submit" id="1" value="1" class="btn btn-primary"></label> &nbsp
          <label><input type="submit" id="2" value="2" class="btn btn-success"></label> &nbsp
          <label><input type="submit" id="3" value="3" class="btn btn-info"></label></td>
      <td>
        <input type="submit" name="agregar" value="Agregar">&nbsp
        <input type="submit" name="eliminar" value="Eliminar">&nbsp
        <input type="submit" name="modificar" value="Modificar">
      </td>
    </tr>
    <tr>
      <td>5</td>
      <td><input type="text" name="run" placeholder="12345678" required="" "></td>
      <td>"Paciente"</td>
      <td>16:30</td>
      <td><select class="custom-select">
        <option>Seleccione</option>
        <option>Masoterapia</option>
        <option>Auriculoterapia</option>
        <option>Pedras Calientes</option>
        <option>Reflexologia</option>
      </select></td>
      <td>Francisca Campos</td>
      <td><label><input type="submit" id="cbox1" value="1" class="btn btn-primary"></label> &nbsp
          <label><input type="submit" id="cbox1" value="2" class="btn btn-success"></label> &nbsp
          <label><input type="submit" id="cbox1" value="3" class="btn btn-info"></label>
      </td>
      <td>
        <input type="submit" name="agregar" value="Agregar">&nbsp
        <input type="submit" name="eliminar" value="Eliminar">&nbsp
        <input type="submit" name="modificar" value="Modificar">
      </td>
    </tr>
    <tr class="table-info">
      <td>6</td>
      <td><input type="text" name="run" placeholder="12345678" required="" "></td>
      <td>"Paciente"</td>
      <td>16:30</td>
      <td><select class="custom-select">
        <option>Seleccione</option>
        <option>Masoterapia</option>
        <option>Auriculoterapia</option>
        <option>Pedras Calientes</option>
        <option>Reflexologia</option>
      </select></td>
      <td>Estefania Rubilar</td>
      <td><label><input type="submit" id="cbox1" value="1" class="btn btn-primary"></label> &nbsp
          <label><input type="submit" id="cbox1" value="2" class="btn btn-success"></label> &nbsp
          <label><input type="submit" id="cbox1" value="3" class="btn btn-info"></label>
      </td>
      <td>
        <input type="submit" name="agregar" value="Agregar">&nbsp
        <input type="submit" name="eliminar" value="Eliminar">&nbsp
        <input type="submit" name="modificar" value="Modificar">
      </td>
    </tr>
	<tr>
      <td>7</td>
      <td><input type="text" name="run" placeholder="12345678" required="" "></td>
      <td>"Paciente"</td>
      <td>17:00</td>
      <td><select class="custom-select">
        <option>Seleccione</option>
        <option>Masoterapia</option>
        <option>Auriculoterapia</option>
        <option>Pedras Calientes</option>
        <option>Reflexologia</option>
      </select></td>
      <td>Maria Valdes</td>
      <td><label><input type="submit" id="1" value="1" class="btn btn-primary"></label> &nbsp
          <label><input type="submit" id="2" value="2" class="btn btn-success"></label> &nbsp
          <label><input type="submit" id="3" value="3" class="btn btn-info"></label></td>
      <td>
        <input type="submit" name="agregar" value="Agregar">&nbsp
        <input type="submit" name="eliminar" value="Eliminar">&nbsp
        <input type="submit" name="modificar" value="Modificar">
      </td>
    </tr>
    <tr>
      <td>8</td>
      <td><input type="text" name="run" placeholder="12345678" required="" "></td>
      <td>"Paciente"</td>
      <td>17:00</td>
      <td><select class="custom-select">
        <option>Seleccione</option>
        <option>Masoterapia</option>
        <option>Auriculoterapia</option>
        <option>Pedras Calientes</option>
        <option>Reflexologia</option>
      </select></td>
      <td>Francisca Campos</td>
      <td><label><input type="submit" id="cbox1" value="1" class="btn btn-primary"></label> &nbsp
          <label><input type="submit" id="cbox1" value="2" class="btn btn-success"></label> &nbsp
          <label><input type="submit" id="cbox1" value="3" class="btn btn-info"></label>
      </td>
      <td>
        <input type="submit" name="agregar" value="Agregar">&nbsp
        <input type="submit" name="eliminar" value="Eliminar">&nbsp
        <input type="submit" name="modificar" value="Modificar">
      </td>
    </tr>
    <tr class="table-info">
      <td>9</td>
      <td><input type="text" name="run" placeholder="12345678" required="" "></td>
      <td>"Paciente"</td>
      <td>17:00</td>
      <td><select class="custom-select">
        <option>Seleccione</option>
        <option>Masoterapia</option>
        <option>Auriculoterapia</option>
        <option>Pedras Calientes</option>
        <option>Reflexologia</option>
      </select></td>
      <td>Estefania Rubilar</td>
      <td><label><input type="submit" id="cbox1" value="1" class="btn btn-primary"></label> &nbsp
          <label><input type="submit" id="cbox1" value="2" class="btn btn-success"></label> &nbsp
          <label><input type="submit" id="cbox1" value="3" class="btn btn-info"></label>
      </td>
      <td>
        <input type="submit" name="agregar" value="Agregar">&nbsp
        <input type="submit" name="eliminar" value="Eliminar">&nbsp
        <input type="submit" name="modificar" value="Modificar">
      </td>
    </tr>
	<tr>
      <td>10</td>
      <td><input type="text" name="run" placeholder="12345678" required="" "></td>
      <td>"Paciente"</td>
      <td>17:30</td>
      <td><select class="custom-select">
        <option>Seleccione</option>
        <option>Masoterapia</option>
        <option>Auriculoterapia</option>
        <option>Pedras Calientes</option>
        <option>Reflexologia</option>
      </select></td>
      <td>Maria Valdes</td>
      <td><label><input type="submit" id="1" value="1" class="btn btn-primary"></label> &nbsp
          <label><input type="submit" id="2" value="2" class="btn btn-success"></label> &nbsp
          <label><input type="submit" id="3" value="3" class="btn btn-info"></label></td>
      <td>
        <input type="submit" name="agregar" value="Agregar">&nbsp
        <input type="submit" name="eliminar" value="Eliminar">&nbsp
        <input type="submit" name="modificar" value="Modificar">
      </td>
    </tr>
    <tr>
      <td>11</td>
      <td><input type="text" name="run" placeholder="12345678" required="" "></td>
      <td>"Paciente"</td>
      <td>17:30</td>
      <td><select class="custom-select">
        <option>Seleccione</option>
        <option>Masoterapia</option>
        <option>Auriculoterapia</option>
        <option>Pedras Calientes</option>
        <option>Reflexologia</option>
      </select></td>
      <td>Francisca Campos</td>
      <td><label><input type="submit" id="cbox1" value="1" class="btn btn-primary"></label> &nbsp
          <label><input type="submit" id="cbox1" value="2" class="btn btn-success"></label> &nbsp
          <label><input type="submit" id="cbox1" value="3" class="btn btn-info"></label>
      </td>
      <td>
        <input type="submit" name="agregar" value="Agregar">&nbsp
        <input type="submit" name="eliminar" value="Eliminar">&nbsp
        <input type="submit" name="modificar" value="Modificar">
      </td>
    </tr>
    <tr class="table-info">
      <td>12</td>
      <td><input type="text" name="run" placeholder="12345678" required="" "></td>
      <td>"Paciente"</td>
      <td>17:30</td>
      <td><select class="custom-select">
        <option>Seleccione</option>
        <option>Masoterapia</option>
        <option>Auriculoterapia</option>
        <option>Pedras Calientes</option>
        <option>Reflexologia</option>
      </select></td>
      <td>Estefania Rubilar</td>
      <td><label><input type="submit" id="cbox1" value="1" class="btn btn-primary"></label> &nbsp
          <label><input type="submit" id="cbox1" value="2" class="btn btn-success"></label> &nbsp
          <label><input type="submit" id="cbox1" value="3" class="btn btn-info"></label>
      </td>
      <td>
        <input type="submit" name="agregar" value="Agregar">&nbsp
        <input type="submit" name="eliminar" value="Eliminar">&nbsp
        <input type="submit" name="modificar" value="Modificar">
      </td>
    </tr>
  </tbody>
</table> 
</div>


</body>
</html>