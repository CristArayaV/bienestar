<html>
<head>
  <link rel="stylesheet" href="../css/bootstrap.css">

  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a href=""><img src="../img/flor.jpg" height="60p"></a>
    </div>
    
  </div>
</nav>
</head>
<body>
<center>
  <legend>Encuesta de Satisfaccion sala bienestar</legend>
</center>
  <div>
    <center>
      <font color="navy" face="Comic Sans MS,arial,verdana" size=4>En una escala del 1 al 5, siendo 1 Muy Deficiente; 2 Deficiente; 3 Regular; 4 Positivo y </font></br>
      <font color="navy" face="Comic Sans MS,arial,verdana" size=4>5 Muy Positivo. Como evaluaría UD. los siguientes aspectos de la sala de BIENESTAR</font>
      <br><br>

    <ul>
      <li><font color="teal">El Sistema de Agendamiento y la atención</font></li>
     <div class="form-group">
      <div class="custom-control custom-radio">

      <input id="ev1" name="ev1" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev1">1</label>

      <input id="ev1" name="ev1" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev1">2</label>

      <input id="ev1" name="ev1" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev1">3</label>

      <input id="ev1" name="ev1" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev1">4</label>

      <input id="ev1" name="ev1" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev1">5</label>

      </div>
    </div>
    </ul>
    <ul>
      <li><font color="teal">La Infraestructura de la sala</font></li>
      <div class="form-group">
      <div class="custom-control custom-radio">

      <input id="ev2" name="ev2" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev2">1</label>

      <input id="ev2" name="ev2" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev2">2</label>

      <input id="ev2" name="ev2" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev2">3</label>

      <input id="ev2" name="ev2" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev2">4</label>

      <input id="ev2" name="ev2" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev2">5</label>

      </div>

    </ul>
    <ul>
      <li><font color="teal">La prestación recibida, ¿Cumple el Objetivo y la expectativa de la atención?</font></li>
      <div class="form-group">
      <div class="custom-control custom-radio">

      <input id="ev3" name="ev3" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev3">1</label>

      <input id="ev3" name="ev3" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev3">2</label>

      <input id="ev3" name="ev3" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev3">3</label>

      <input id="ev3" name="ev3" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev3">4</label>

      <input id="ev3" name="ev3" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev3">5</label>

      </div>
    </ul>
    <ul>
      <li><font color="teal">La actitud del estudiante, ¿Es la adecuada en el contexto de la sala de Bienestar?</font></li>
      <div class="form-group">
      <div class="custom-control custom-radio">

      <input id="ev4" name="ev4" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev4">1</label>

      <input id="ev4" name="ev4" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev4">2</label>

      <input id="ev4" name="ev4" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev4">3</label>

      <input id="ev4" name="ev4" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev4">4</label>

      <input id="ev4" name="ev4" class="custom-control-input" type="radio">
        <label class="custom-control-label" for="ev4">5</label>

      </div>
    </ul>

    <div class="form-group">
      <label for="comentario">Observaciones y Comentarios</label>
      <textarea class="form-control" id="comentario" rows="3"></textarea>
    </div>
</table>
</body>
</html>