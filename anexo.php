<html>
<head>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../con/bootstrap.css">
  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a href="index.html"><img src="../img/flor.jpg" height="60p"></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li></li>
        <li><a>Alumno: Ambar Duran<span class="sr-only">(current)</span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="">Cerrar Sesión</a></li>
      </ul>
    </div>
  </div>
</nav>
</head>
<body>
  <center>
  <table border="1" class="table table-hover">
    <form action="">
      <tr>
        <th colspan="2">Datos Paciente</th>
        
      </tr>
      <tr>
        <th>Paciente</th>
        <th>Rut</th>
        
      </tr>
      <tr>
        <td><input type="text" name="nombre" id="nom" disabled="" placeholder="Juan Aranda"></td>
        <td><input type="text" name="rut" id="run" disabled="" placeholder="8.888.888-8"></td>
        
      </tr>
      <tr>
        <th>Edad</th>
        <th>Telefono</th>
      </tr>
    <tr>
      <td><input type="number" name="edad" id="eda" disabled="" placeholder="80"></td>
      <td><input type="number" name="fono" id="fon" disabled="" placeholder="9-78956734"></td>
    </tr>
    <tr>
      <th>Fecha Nacimiento</th>
      <th> Fecha:</th>
    </tr>
    <tr>
      <td><input type="date" name="fecha" id="fn" disabled="" placeholder=sysdate></td>
      <td></td>
    </tr>
    </form>
  </table>

  <table border="1" class="table table-hover">
    <form action="">
    <tr>
      <td>
        <div class="form-group">
          <label for="txtEnf">I.- Motivo de Consulta, Evaluacion</label>
          <textarea class="form-control" id="txtMot" rows="5"></textarea>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="form-group">
          <label for="txtEnf">II.- Tratamiento</label>
          <textarea class="form-control" id="txtTra" rows="5"></textarea>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="form-group">
          <label for="txtEnf">III.-Observaciones y Comentarios</label>
          <textarea class="form-control" id="txtOyC" rows="5"></textarea>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <div class="form-group">
          <label for="txtEnf">IV.- Indicaciones para el Domicilio</label>
          <textarea class="form-control" id="txtInd" rows="5"></textarea>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <center>
        <button type="submit" id="btnGuardar" class="btn btn-primary">Guardar</button>
        <button type="clean" id="btnLimpiar" class="btn btn-warning">Limpiar</button>
      </center>
      </td>
    </tr>
    </form>
  </table>  
</center>
</body>
</html>