<html>
<head>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/mycss.css">

</head>
<body>
 <?php  
  include 'navMenu.php';
  if (!$_SESSION['tip']=="4") {
    header("Location: login.php");

}
 
  /**$fecha = date(d-m-Y,time());**/
   
  ?>

 <div class="container">
  <table>
    <form action="" method="POST">
  <center>
  <table border="1" class="table table-hover">
      <tr>
        <th colspan="2"> I.- Datos Personales</th>
      </tr>
      <tr>
        <th>Paciente : </th>
        <th>Rut : </th>
        
      </tr>
      <tr>
        <td><input type="text" name="nombre" id="nom" disabled="" ="" placeholder="Juan Aranda"></td>
        <td><input type="text" name="rut" id="run" placeholder="8888888"> <input type="text" name="dv" id="dv" placeholder="K" size="1" ></td>
        
      </tr>
      <tr>
        <th>Edad : </th>
        <th>Telefono : </th>
      </tr>
    <tr>
      <td><input type="number" name="edad" id="eda" disabled="" placeholder="80"></td>
      <td><input type="number" name="fono" id="fon" disabled="" placeholder="9-78956734"></td>
    </tr>
    <tr>
      <th>Fecha Nacimiento : </th>
      <th> Fecha Actual :</th>
    </tr>
    <tr>
      <td><input type="date" name="fecha" id="fecha"  placeholder=sysdate></td>
      <td><input type="date" name="incial_fecha" id="incial_fecha" placeholder=sysdate value=""></td>
    </tr>
  </table>
  <br>
  <table border="1" class="table table-hover">
    <tr>
      <td>
        <div class="form-group">
          <label for="txtEnf">II.- Enfermedades Cronicas o Relevantes, Lesiones, Operaciones Actuales y Anteriores con o sin Tratamiento</label>
          <textarea class="form-control" id="txtEnf" rows="5"></textarea>
        </div>
      </td>
    </tr>
  </table><br>
  <table border="1" class="table table-hover">
    <tr>
      <th colspan="1">III.- ESTILO DE VIDA</th>
      <th colspan="1">Si</th>
      <th colspan="1">NO</th>
      <th colspan="2">Observaciones</th>
    </tr>
    <tr>
      <td>Actividad Fisica</td>
      <td>  
        <div class="form-group">
          <div class="custom-control custom-radio">
          <input id="rbAF1" name="customRadio1" class="custom-control-input" checked="" type="radio">
        </div>
      </div>
    </td>
      <td><div class="custom-control custom-radio">
      <input id="rbAF2" name="customRadio1" class="custom-control-input" type="radio">
    </div></td>
      <td><input type="text" name="txtAF"></td>
    </tr>
    <tr>
      <td>Trabajo</td>
      <td>  
        <div class="form-group">
          <div class="custom-control custom-radio">
          <input id="customRadio1" name="customRadio" class="custom-control-input" checked="" type="radio">
        </div>
      </div>
    </td>
      <td><div class="custom-control custom-radio">
      <input id="customRadio2" name="customRadio" class="custom-control-input" type="radio">
    </div></td>
      <td><input type="text" name="txtTrab"></td>
    </tr>
    <tr>
      <td>Estado Emocional</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="text" name="txtEstado"></td>
    </tr>
    <tr>
      <td> Calidad del Sueño</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="text" name="txtCalidad"></td>
    </tr>
  </table><br>
<table border="1" class="table table-hover">
  <tr>
    <th colspan="4">IV.- Desarrollo de la Consulta</th>
  </tr>   
  <tr>
    <td colspan="1">¿Como Se Origino?</td>
    <td colspan="3"><input type="text" name="txtCalidad"></td>
  </tr>
  <tr>
    <td colspan="1">¿Hace Cuanto'</td>
    <td colspan="3"><input type="text" name="txtCalidad"></td>
  </tr>
  <tr>
    <td colspan="1">¿En que ocación le duele?</td>
    <td colspan="3"><input type="text" name="txtCalidad"></td>
  </tr>
  <tr>
    <td colspan="1">¿Limitacion de Movilidad?</td>
    <td colspan="3"><input type="text" name="txtCalidad"></td>
  </tr>
  <tr>
    <td colspan="1">Tratamientos actuales o previos</td>
    <td colspan="3"><input type="text" name="txtCalidad"></td>
  </tr>
  <tr>
    <td></td>
    <td><button type="button" class="btn btn-primary" id="btnGuardar">Guardar</button>  <button type="clean" class="btn btn-primary">Limpiar</button></td>
  </tr>

</table>
</center>
</form>
</table>
</div>
</body>
</html>